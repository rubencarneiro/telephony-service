# Albanian translation for telephony-service
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the telephony-service package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: telephony-service\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-15 21:05+0000\n"
"PO-Revision-Date: 2020-12-19 13:26+0000\n"
"Last-Translator: Enkli Ylli <eylli@yahoo.com>\n"
"Language-Team: Albanian <https://translate.ubports.com/projects/ubports/"
"telephony-service/sq/>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-01-04 06:02+0000\n"

#: indicator/messagingmenu.cpp:373
#, qt-format
msgid "%1 missed call"
msgid_plural "%1 missed calls"
msgstr[0] "%1 thirrje e humbur"
msgstr[1] "%1 thirrje të humbura"

#: indicator/messagingmenu.cpp:459
#, qt-format
msgid "%1 voicemail message"
msgid_plural "%1 voicemail messages"
msgstr[0] ""
msgstr[1] ""

#: indicator/metrics.cpp:48
#, qt-format
msgid "<b>%1</b> calls made today"
msgstr "<b>%1</b> thirrje të bëra sot"

#: indicator/metrics.cpp:46
#, qt-format
msgid "<b>%1</b> calls received today"
msgstr "<b>%1</b> thirrje të ardhura sot"

#: indicator/metrics.cpp:44
#, qt-format
msgid "<b>%1</b> text messages received today"
msgstr ""

#: indicator/metrics.cpp:42
#, qt-format
msgid "<b>%1</b> text messages sent today"
msgstr ""

#: approver/approver.cpp:523
msgid "Accept"
msgstr "Prano"

#: indicator/textchannelobserver.cpp:499
#, qt-format
msgid "Attachment: %1 audio clip"
msgid_plural "Attachments: %1 audio clips"
msgstr[0] ""
msgstr[1] ""

#: indicator/textchannelobserver.cpp:497
#, qt-format
msgid "Attachment: %1 contact"
msgid_plural "Attachments: %1 contacts"
msgstr[0] ""
msgstr[1] ""

#: indicator/textchannelobserver.cpp:501
#, qt-format
msgid "Attachment: %1 file"
msgid_plural "Attachments: %1 files"
msgstr[0] ""
msgstr[1] ""

#: indicator/textchannelobserver.cpp:493
#, qt-format
msgid "Attachment: %1 image"
msgid_plural "Attachments: %1 images"
msgstr[0] ""
msgstr[1] ""

#: indicator/textchannelobserver.cpp:495
#, qt-format
msgid "Attachment: %1 video"
msgid_plural "Attachments: %1 videos"
msgstr[0] ""
msgstr[1] ""

#: indicator/authhandler.cpp:72
msgid "Authentication failed. Do you want to review your credentials?"
msgstr ""

#: indicator/messagingmenu.cpp:309
msgid "Call back"
msgstr ""

#: approver/approver.cpp:469 approver/approver.cpp:484
msgid "Caller number is not available"
msgstr ""

#: approver/approver.cpp:481
#, qt-format
msgid "Calling from %1"
msgstr "Thirrje nga %1"

#: approver/approver.cpp:475
msgid "Calling from private number"
msgstr "Thirrje nga numër privat"

#: approver/approver.cpp:478
msgid "Calling from unknown number"
msgstr "Thirrje nga numër i panjohur"

#: indicator/ussdindicator.cpp:142
msgid "Cancel"
msgstr "Anullo"

#: indicator/textchannelobserver.cpp:225
msgid "Deactivate flight mode and try again from the messaging application."
msgstr ""

#: approver/approver.cpp:545
msgid "Decline"
msgstr ""

#: approver/approver.cpp:536
msgid "End + Answer"
msgstr ""

#: approver/approver.cpp:522
msgid "Hold + Answer"
msgstr ""

#: indicator/messagingmenu.cpp:314
msgid "I missed your call - can you call me now?"
msgstr ""

#: indicator/messagingmenu.cpp:317
msgid "I'll be 20 minutes late."
msgstr "Do vonohem 20 minuta."

#: approver/approver.cpp:93
msgid "I'm busy at the moment. I'll call later."
msgstr ""

#: indicator/messagingmenu.cpp:316
msgid "I'm busy at the moment. I'll call you later."
msgstr ""

#: approver/approver.cpp:94
msgid "I'm running late, on my way now."
msgstr ""

#: indicator/messagingmenu.cpp:315
msgid "I'm running late. I'm on my way."
msgstr ""

#: approver/approver.cpp:553
msgid "Message & decline"
msgstr ""

#: indicator/textchannelobserver.cpp:638
#, qt-format
msgid "Message from %1"
msgstr "Mesazh nga %1"

#. TRANSLATORS : %1 is the group name and %2 is the recipient name
#: indicator/messagingmenu.cpp:226 indicator/textchannelobserver.cpp:560
#: indicator/textchannelobserver.cpp:564
#, qt-format
msgid "Message to %1 from %2"
msgstr ""

#. TRANSLATORS : %1 is the recipient name
#: indicator/messagingmenu.cpp:229 indicator/messagingmenu.cpp:233
#: indicator/textchannelobserver.cpp:568 indicator/textchannelobserver.cpp:572
#, qt-format
msgid "Message to group from %1"
msgstr ""

#: indicator/textchannelobserver.cpp:293
msgid "New Group"
msgstr ""

#. TRANSLATORS : message displayed when any error occurred while receiving a MMS (case when cellular-data is off, or any downloading issue). Notify that there was a message, the user can find more about it in the messaging-app.
#: indicator/textchannelobserver.cpp:504
#, fuzzy
#| msgid "View message"
msgid "New MMS message"
msgstr "Shiko mesazhin"

#: indicator/authhandler.cpp:94
msgid "No"
msgstr "Jo"

#: indicator/metrics.cpp:49 indicator/metrics.cpp:51
msgid "No calls made today"
msgstr "Asnjë thirrje e bërë sot"

#: indicator/metrics.cpp:47
msgid "No calls received today"
msgstr ""

#: indicator/metrics.cpp:45
msgid "No text messages received today"
msgstr ""

#: indicator/metrics.cpp:43
msgid "No text messages sent today"
msgstr ""

#: indicator/ussdindicator.cpp:116 indicator/textchannelobserver.cpp:365
msgid "Ok"
msgstr "Ok"

#: approver/approver.cpp:456
#, qt-format
msgid "On [%1]"
msgstr ""

#: indicator/telephony-service-call.desktop.in:3
msgid "Phone Calls"
msgstr ""

#: approver/approver.cpp:95
msgid "Please call me back later."
msgstr ""

#: indicator/textchannelobserver.cpp:793
msgid "Please, select a SIM card:"
msgstr "Zgjidh një skedë SIM:"

#: approver/approver.cpp:460 indicator/messagingmenu.cpp:378
msgid "Private number"
msgstr ""

#: indicator/ussdindicator.cpp:119
msgid "Reply"
msgstr "Përgjigju"

#: indicator/displaynamesettings.cpp:35
#, qt-format
msgid "SIM %1"
msgstr "SIM %1"

#: indicator/telephony-service-sms.desktop.in:3
msgid "SMS"
msgstr "SMS"

#: indicator/textchannelobserver.cpp:371
msgid "Save"
msgstr "Ruaj"

#: indicator/messagingmenu.cpp:151 indicator/messagingmenu.cpp:257
#: indicator/messagingmenu.cpp:324
msgid "Send"
msgstr "Dërgo"

#: indicator/messagingmenu.cpp:318
msgid "Sorry, I'm still busy. I'll call you later."
msgstr ""

#: indicator/metrics.cpp:50
#, qt-format
msgid "Spent <b>%1</b> minutes in calls today"
msgstr ""

#: indicator/messagingmenu.cpp:83 indicator/messagingmenu.cpp:87
msgid "Telephony Service"
msgstr ""

#: approver/main.cpp:46
msgid "Telephony Service Approver"
msgstr ""

#: indicator/main.cpp:53
msgid "Telephony Service Indicator"
msgstr ""

#: indicator/textchannelobserver.cpp:234
msgid "The message could not be sent"
msgstr ""

#: indicator/textchannelobserver.cpp:228
msgid "Try again from the messaging application."
msgstr ""

#: approver/approver.cpp:65
msgid "Unknown caller"
msgstr ""

#: approver/approver.cpp:463 indicator/messagingmenu.cpp:208
#: indicator/messagingmenu.cpp:382 indicator/textchannelobserver.cpp:529
msgid "Unknown number"
msgstr ""

#: indicator/textchannelobserver.cpp:223
msgid "Unlock your sim card and try again from the messaging application."
msgstr ""

#: indicator/textchannelobserver.cpp:326
msgid "View Group"
msgstr ""

#: indicator/textchannelobserver.cpp:247 indicator/textchannelobserver.cpp:602
msgid "View message"
msgstr "Shiko mesazhin"

#: indicator/messagingmenu.cpp:464
msgid "Voicemail"
msgstr ""

#: indicator/messagingmenu.cpp:456
msgid "Voicemail messages"
msgstr ""

#: indicator/authhandler.cpp:91
msgid "Yes"
msgstr "Po"

#: indicator/textchannelobserver.cpp:307
msgid "You joined a new group"
msgstr ""

#. TRANSLATORS : %1 is the group name
#: indicator/textchannelobserver.cpp:304
#, qt-format
msgid "You joined group %1"
msgstr ""

#. TRANSLATORS : %1 is the group name
#: indicator/textchannelobserver.cpp:300
#, qt-format
msgid "You joined group %1 "
msgstr ""
